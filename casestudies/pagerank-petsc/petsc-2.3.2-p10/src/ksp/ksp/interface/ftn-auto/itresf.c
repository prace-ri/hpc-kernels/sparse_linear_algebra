#include "petsc.h"
#include "petscfix.h"
/* itres.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petscksp.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define kspunwindpreconditioner_ KSPUNWINDPRECONDITIONER
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define kspunwindpreconditioner_ kspunwindpreconditioner
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
void PETSC_STDCALL   kspunwindpreconditioner_(KSP ksp,Vec vsoln,Vec vt1, int *__ierr ){
*__ierr = KSPUnwindPreconditioner(
	(KSP)PetscToPointer((ksp) ),
	(Vec)PetscToPointer((vsoln) ),
	(Vec)PetscToPointer((vt1) ));
}
#if defined(__cplusplus)
}
#endif
